package com.test.springcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.springcloud.dao.PaymentMapper;
import com.test.springcloud.entities.Payment;
import com.test.springcloud.service.PaymentService;

public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements PaymentService {
}

package com.test.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.springcloud.entities.Payment;

public interface PaymentService extends IService<Payment> {
}

package com.test.springcloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.springcloud.entities.Payment;

public interface PaymentMapper extends BaseMapper<Payment> {
}
